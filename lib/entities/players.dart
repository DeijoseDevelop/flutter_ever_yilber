import 'dart:convert';

List<Player> playerFromJson(String str) => List<Player>.from(json.decode(str).map((x) => Player.fromJson(x)));

String playerToJson(List<Player> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Player {
    Player({
        this.name,
        this.bornDate,
        this.age,
        this.goals,
        this.assists,
        this.country,
        this.position,
        this.playedGames,
        this.titular,
        this.minutes,
        this.goalsWithoutPenal,
        this.executedPenal,
        this.yellowCards,
        this.redCards,
        this.coverageGoals,
        this.coverageAssists,
        this.coverageGoalsAssists,
        this.coverageGoalsWithoutPenal,
        this.coverageGoalsAssistsWithoutPenal,
    });

    String? name;
    int? bornDate;
    int? age;
    int? goals;
    int? assists;
    String? country;
    String? position;
    int? playedGames;
    int? titular;
    int? minutes;
    int? goalsWithoutPenal;
    int? executedPenal;
    int? yellowCards;
    int? redCards;
    double? coverageGoals;
    double? coverageAssists;
    double? coverageGoalsAssists;
    double? coverageGoalsWithoutPenal;
    double? coverageGoalsAssistsWithoutPenal;

    Player copyWith({
        String? name,
        int? bornDate,
        int? age,
        int? goals,
        int? assists,
        String? country,
        String? position,
        int? playedGames,
        int? titular,
        int? minutes,
        int? goalsWithoutPenal,
        int? executedPenal,
        int? yellowCards,
        int? redCards,
        double? coverageGoals,
        double? coverageAssists,
        double? coverageGoalsAssists,
        double? coverageGoalsWithoutPenal,
        double? coverageGoalsAssistsWithoutPenal,
    }) =>
        Player(
            name: name ?? this.name,
            bornDate: bornDate ?? this.bornDate,
            age: age ?? this.age,
            goals: goals ?? this.goals,
            assists: assists ?? this.assists,
            country: country ?? this.country,
            position: position ?? this.position,
            playedGames: playedGames ?? this.playedGames,
            titular: titular ?? this.titular,
            minutes: minutes ?? this.minutes,
            goalsWithoutPenal: goalsWithoutPenal ?? this.goalsWithoutPenal,
            executedPenal: executedPenal ?? this.executedPenal,
            yellowCards: yellowCards ?? this.yellowCards,
            redCards: redCards ?? this.redCards,
            coverageGoals: coverageGoals ?? this.coverageGoals,
            coverageAssists: coverageAssists ?? this.coverageAssists,
            coverageGoalsAssists: coverageGoalsAssists ?? this.coverageGoalsAssists,
            coverageGoalsWithoutPenal: coverageGoalsWithoutPenal ?? this.coverageGoalsWithoutPenal,
            coverageGoalsAssistsWithoutPenal: coverageGoalsAssistsWithoutPenal ?? this.coverageGoalsAssistsWithoutPenal,
        );

    factory Player.fromJson(Map<String, dynamic> json) => Player(
        name: json["name"],
        bornDate: json["born_date"],
        age: json["age"],
        goals: json["goals"],
        assists: json["assists"],
        country: json["country"],
        position: json["position"],
        playedGames: json["played_games"],
        titular: json["titular"],
        minutes: json["minutes"],
        goalsWithoutPenal: json["goals_without_penal"],
        executedPenal: json["executed_penal"],
        yellowCards: json["yellow_cards"],
        redCards: json["red_cards"],
        coverageGoals: json["coverage_goals"].toDouble(),
        coverageAssists: json["coverage_assists"].toDouble(),
        coverageGoalsAssists: json["coverage_goals_assists"].toDouble(),
        coverageGoalsWithoutPenal: json["coverage_goals_without_penal"].toDouble(),
        coverageGoalsAssistsWithoutPenal: json["coverage_goals_assists_without_penal"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "born_date": bornDate,
        "age": age,
        "goals": goals,
        "assists": assists,
        "country": country,
        "position": position,
        "played_games": playedGames,
        "titular": titular,
        "minutes": minutes,
        "goals_without_penal": goalsWithoutPenal,
        "executed_penal": executedPenal,
        "yellow_cards": yellowCards,
        "red_cards": redCards,
        "coverage_goals": coverageGoals,
        "coverage_assists": coverageAssists,
        "coverage_goals_assists": coverageGoalsAssists,
        "coverage_goals_without_penal": coverageGoalsWithoutPenal,
        "coverage_goals_assists_without_penal": coverageGoalsAssistsWithoutPenal,
    };
}
